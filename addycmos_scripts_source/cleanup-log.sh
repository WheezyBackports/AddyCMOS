## Licensed under GPLv2
## Author : WheezyBackports
#!/bin/sh

LOGPATH="/srv/xmrig-mo/log.txt"

if [ -f $LOGPATH ]; then
	LAST_ALGO_PERF=$(grep "job" $LOGPATH | tail -n1 && grep "miner" $LOGPATH | tail -n1)
	head -n1 /dev/null >$LOGPATH
	echo $LAST_ALGO_PERF >$LOGPATH
fi
