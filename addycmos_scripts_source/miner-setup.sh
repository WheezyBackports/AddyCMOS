## Licensed under GPLv2
## Author : WheezyBackports
#!/bin/sh

## Path Variables
SRVPATH=/srv/xmrig-mo
CONFPATH=$SRVPATH/conf.d
LOGPATH=$SRVPATH/log.txt;

## Command line arguments for miner configuration
for ARGUMENT in "$@"
do

    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)   

    case "$KEY" in
            WALLETADDR)              WALLETADDR=${VALUE} ;;
            WORKERNAME)    	     WORKERNAME=${VALUE} ;;     
	    POOLADDR)		     POOLADDR=${VALUE}	 ;;
	    ALGO)		     ALGO=${VALUE}	 ;;
	    NOFEE)		     NOFEE=${VALUE}	 ;;
	    LOG)		     LOG=${VALUE}	 ;;
	    USECONFIG)		     USECONFIG=${VALUE}	 ;;
            *)   
    esac    


done

## Copies template config to new config for miner and deletes old log file if it exists
presetup () {
	cp $CONFPATH/template-config.json $CONFPATH/config.json && echo "Copied new config file from template."
	rm $LOGPATH 2>&-
}

## Runs the basic mining setup commands
configsetup () {
	## Only ran when a configuration file is specified
	if [ "$USECONFIG" != '' ]; then
		echo "Using script config : $USECONFIG"
		source $USECONFIG
	fi
	
	## Only works on pools where you can specify algorithm in the worker name
	if [ "$ALGO" != '' ]; then
		echo "Set algorithm to : $ALGO"
		WORKERNAME="$WORKERNAME~$ALGO"
	fi
	
	## User specified wallet address gets added to new miner config
	if [ "$WALLETADDR" != '' ]; then 
		echo "Using wallet address : $WALLETADDR";
		sed -i "s/YOUR_WALLET_ADDRESS/$WALLETADDR/g" $CONFPATH/config.json && echo "Wrote wallet address to config.";
		SETUPINIT=yes
	else
		echo "You need to specify a wallet address!"
	fi

	## User specified worker name gets added to new miner config
	if [ "$WORKERNAME" != '' ]; then 
		echo "Using worker name :  $WORKERNAME";
		sed -i "s/\"x/\"$WORKERNAME/g" $CONFPATH/config.json && echo "Wrote worker name to config."
	else
		echo "You need to specify a worker name!"
	fi

	## Change pool to user specified pool
	## If no pool is specified use gulf.moneroocean.stream:10128 as default
	if [ "$POOLADDR" != '' ]; then
		sed -i "s/gulf.moneroocean.stream:10128/$POOLADDR/g" $CONFPATH/config.json && echo "Changed pool address to : $POOLADDR"
	fi
}

## Extra options that aren't necessary for the miner to run
extraopts () {
	## Removes the dev fee for AddyCMOS
	if [ "$NOFEE" = 'y' ]; then 
		sed -i "s/%0.5%42dAoQQFiT31Z86XPhrj7Y1nKwW8jvMDpTPZZTV1SfgS7vvBt24xgRneuQ3gmCuhQVUmKtMnVNEnQ8K589g2eFtTEam8HqU//g" $CONFPATH/config.json && echo "Removed AddyCMOS dev fee." 
	fi

	## Enables logging by telling the config file what the log path is and creates an empty log file
	## When logging is enabled extra features in simpmon are also enabled for monitoring mining
	if [ "$LOG" = 'y' ]; then
		sed -i "s+\"log-file\"\: null,+\"log-file\"\: \"$LOGPATH\",+g" $CONFPATH/config.json
		touch $LOGPATH
	fi
}

## Stops, disables, enables, and starts the xmrigboot service
## xmrigboot is only stopped and disabled if it is already running and enabled
initscript () {
	if [ "$SETUPINIT" = 'yes' ]; then
		doas rc-update del xmrigboot 2>&- 1>&-
		doas rc-service xmrigboot stop 2>&- 1>&-
		echo "Setting up init script.";
		doas rc-update add xmrigboot default;
		doas rc-service xmrigboot start;
	else
		echo "Setup init failed!"
	fi
}

## Runs all the defined functions
presetup
configsetup
extraopts
initscript
